# -*- coding: utf-8 -*-

from flask import Flask, session, request, g, make_response, send_file, Blueprint, json
from datetime import datetime, timedelta
import os
import io
import re
import traceback

import psycopg2

from flask_cors import CORS 
from flask_session import Session

from event import Event, NotOpenedException
from jukebox import Jukebox
from loginAndSeat import LoginAndSeat
from matchRecord import MatchRecord
from playerManager import PlayerManager
from coupon import CouponManager
from introMovie import IntroMovie
from matchManager import MatchManager
from mvpVote import MvpVote
from selfie import Selfie
from point import PointManager
import lib
from decorators import login_required, admin_required

DATABASE = "host=localhost port=5432 dbname=xsync user=xsync password=postgres"
IS_PRODUCTION = True

blueprint = Blueprint('bp_ver1', __name__)

def connect_db():
    """
    DB connector 함수
    """
    return psycopg2.connect(DATABASE)

@blueprint.before_request
def before_request():
    """
    모든 API 실행 전 실행하는 부분. 여기서는 DB 연결
    """
    g.db = connect_db()

@blueprint.teardown_request
def teardown_request(exception):
    """
    모든 API 실행 후 실행하는 부분. 여기서는 DB 연결종료.
    """
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

########### Main API ###############
@blueprint.route('/ping', methods=["GET"])
def ping():
    cursor = g.db.cursor()
    cursor.execute("SELECT * FROM XSYNC.D_USERS LIMIT 1")
    return make_response(json.jsonify(message="OK"), 200)

@blueprint.route('/user/login', methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return make_response(json.jsonify(
            is_loggedIn=session.get('is_loggedIn', False)
          , user_id=session.get('user_id')
          , user_name=session.get('user_name')
          , user_phone=session.get('user_phone')
          , touching_id=session.get('touching_id')
          , match_id=session.get('match_id')
          , is_seatInputted=session.get('is_seatInputted')
          , device_category=session.get('device_category')
          , token=session.get('token')
          ), 200 if session.get('is_loggedIn', False) == True else 401)

    elif request.method == "POST":
        parameters = lib.parse_request(request)
        name = parameters['user_name']
        phoneNumber = parameters['user_phone']

        check_number = re.search(r'([^0-9]+)', phoneNumber)
        if check_number is not None:
            return make_response(json.jsonify(message="Not right phone number"), 401)

        device_category = parameters.get('device_category', None)
        token = parameters.get('token', None)

        loginAndSeatObj = LoginAndSeat(g.db)
        try:
            info = loginAndSeatObj.loginAndProvideInfo(name, phoneNumber, device_category=device_category, token=token)
        except LoginAndSeat.TouchingNotAvailableException:
            return make_response(json.jsonify(
                message="Not registered in Touching or network is unstable"),
                405)

        pointManager = PointManager(g.db)
        in_touching = pointManager.check_exist(phoneNumber)
        session['touching_id'] = in_touching

        if info['is_loggedIn'] == False:
            return make_response(json.jsonify(message="Not registered"), 401)

        for key, value in info.iteritems():
            session[key] = value

        g.db.commit()
        return make_response(json.jsonify(**info), 200)

@blueprint.route('/user/logout', methods=["GET"])
def logout():
    session.pop('is_loggedIn', None)
    session.pop('user_id', None)
    session.pop('user_name', None)
    session.pop('user_phone', None)
    session.pop('touching_id', None)
    session.pop('match_id', None)
    session.pop('is_seatInputted', None)
    return make_response(json.jsonify(message = "Logged out"), 200)

@lib.login_required
@blueprint.route('/user/seat', methods=["POST", "GET"])
def getAndInputSeat():
    loginAndSeatObj = LoginAndSeat(g.db)

    if request.method == "POST":
        parameters = lib.parse_request(request)
        match_id = parameters['match_id']
        section = parameters['section']
        row = parameters['row']
        seat_number = parameters['seat_number']

        is_inputted = loginAndSeatObj.setSeatInfo(match_id, session['user_id'], section, row, seat_number)
        if is_inputted == True:
            g.db.commit()

        else:
            g.db.rollback()
            return make_response(json.jsonify(
                message="Fail"), 405)

        return make_response(json.jsonify(
            message="OK"), 200)

    elif request.method == "GET":
        match_id = request.args.get('match_id', 1)

        is_seat_inputted, seat_section, seat_row, seat_no = loginAndSeatObj.getSeatInfo(match_id, session['user_id'])
        return make_response(json.jsonify(
            section=seat_section
          , row=seat_row
          , seat_number=seat_no
            ), 200)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/player_list', methods=["GET"])
def getPlayerList(match_id):
    playerManagerObj = PlayerManager(g.db)
    can_get_list, columns, line_up = playerManagerObj.getLineUpForSpecificMatch(match_id)
    data = lib.dbToDict(columns, line_up)
    return make_response(json.jsonify(data=data), 200)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/player_list/<int:player_id>/profile_pic', methods=["GET"])
def getPlayerProfilePic_user(match_id, player_id):
    playerManagerObj = PlayerManager(g.db)
    can_get_list, photo_ext, photo_bin = playerManagerObj.getPlayerProfilePhoto(player_id)
    if can_get_list == False:
        return make_response(json.jsonify(message="Failure"), 400)

    return send_file(io.BytesIO(photo_bin), attachment_filename='profile.{0}'.format(photo_ext))

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/mvp_vote', methods=["POST"])
def mvpVote(match_id):
    mvpVoteObj = MvpVote(g.db)

    parameters = lib.parse_request(request)
    player_id = parameters['player_id']
    user_id = session['user_id']

    is_voted, err_code = mvpVoteObj.voteMvp(user_id, match_id, player_id)
    if is_voted == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)

    else:
        g.db.rollback()
        if err_code == 1:
            return make_response(json.jsonify(message="Fail"), 405)
        else:
            return make_response(json.jsonify(message="Fail"), 406)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/intro_video', methods=["GET"])
def getIntroMovie(match_id):
    introMovieobj = IntroMovie(g.db)
    can_get, video_url = introMovieobj.getMovieUrl(match_id)
    if can_get == True:
        return make_response(json.jsonify(video_url=video_url), 200)
    else:
        return make_response(json.jsonify(message="Not Found"), 404)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/team_status', methods=["GET"])
def getTeamStatus(match_id):
    matchRecord = MatchRecord(g.db)
    can_get, image_url = matchRecord.getTeamRecord(match_id)
    if can_get == True:
        return make_response(json.jsonify(image_url=image_url), 200)
    else:
        return make_response(json.jsonify(message="Not Found"), 404)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/player_status', methods=["GET"])
def getPlayerStatus(match_id):
    matchRecord = MatchRecord(g.db)
    can_get, image_url = matchRecord.getPlayerRecord(match_id)
    if can_get == True:
        return make_response(json.jsonify(image_url=image_url), 200)
    else:
        return make_response(json.jsonify(message="Not Found"), 404)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/event', methods=["GET", "POST"])
def userEvents(match_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    if request.method == "GET":
        can_get, columns, ret = eventObj.getEventsOfMatch(match_id)
        if can_get == True:
            data = lib.dbToDict(columns, ret)
            return make_response(json.jsonify(events=data), 200)

        else:
            return make_response(json.jsonify(message="Fail"), 404)

    elif request.method == "POST":
        parameters = lib.parse_request(request)
        event_id = parameters['event_id']
        user_id = session['user_id']
        try:
            is_joined = eventObj.joinEvent(match_id, event_id, user_id)

        except NotOpenedException:
            g.db.rollback()
            return make_response(json.jsonify(message="Closed"), 405)

        except Exception:
            traceback.print_exc()
            g.db.rollback()
            return make_response(json.jsonify(message="DB Error"), 500)
            
        if is_joined == 0:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        elif is_joined == 1:
            g.db.rollback()
            return make_response(json.jsonify(message="Lack of point"), 407)
        elif is_joined == 2:
            g.db.rollback()
            return make_response(json.jsonify(message="Already participated"), 406)
        elif is_joined == 3:
            g.db.rollback()
            return make_response(json.jsonify(message="Exceeded max participant"), 408)
        elif is_joined == 4:
            g.db.rollback()
            return make_response(json.jsonify(message="Requested cancel"), 409)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/event/<int:event_id>', methods=["DELETE"])
def cancelEvents(match_id, event_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    is_deleted = eventObj.exitEvent(match_id, event_id, session['user_id'])
    if is_deleted == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/event/<int:event_id>/cancel_request', methods=["POST"])
def eventCancelRequest(match_id, event_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    parameters = lib.parse_request(request)
    description = parameters['description']

    is_deleted = eventObj.requestCancel(match_id, event_id, session['user_id'], description)

    if is_deleted == 0:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    elif is_deleted == 1:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)
    elif is_deleted == 2:
        return make_response(json.jsonify(message="Not applied"), 406)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/event/mine', methods=["GET"])
def realUserEvents(match_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    user_id = session['user_id']
    if request.method == "GET":
        can_get, columns, ret = eventObj.getJoinedEventsOfUser(match_id, user_id)
        if can_get == True:
            data = lib.dbToDict(columns, ret)
            return make_response(json.jsonify(events=data), 200)

        else:
            return make_response(json.jsonify(message="Fail"), 404)

@lib.login_required
@blueprint.route('/user/coupon', methods=["GET", "POST"])
def userCoupons():
    couponObj = CouponManager(g.db)
    user_id = session['user_id']

    if request.method == "GET":
        can_get, columns, ret = couponObj.getAssignedCouponPerUser(user_id)
        if can_get == True:
            data = lib.dbToDict(columns, ret)
            return make_response(json.jsonify(coupon=data), 200)

        else:
            return make_response(json.jsonify(message="Fail"), 404)

    elif request.method == "POST":
        parameters = lib.parse_request(request)
        coupon_id = parameters['coupon_id']
        is_joined = couponObj.checkAsUsed(coupon_id, user_id)
        if is_joined == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

@lib.login_required
@blueprint.route('/user/point', methods=["GET"])
def userPoint():
    pointManager = PointManager(g.db, is_production=IS_PRODUCTION)
    cur_point, point_list = pointManager.check_userInfo(session['user_phone'])
    return make_response(json.jsonify(current_point=cur_point, data=point_list), 200)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/jukebox', methods=["POST"])
def requestJukebox(match_id):
    jukeboxObj = Jukebox(g.db)
    parameters = lib.parse_request(request)
    name = parameters['name']
    description = parameters['description']
    song = parameters['song']

    is_input = jukeboxObj.requestSong(match_id, name, description, song)
    if is_input == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.login_required
@blueprint.route('/user/match/<int:match_id>/selfie', methods=["POST"])
def joinSelfieEvent(match_id):
    selfieObj = Selfie(g.db)

    user_id = session['user_id']
    binary = request.files['image_bin']
    ext = binary.filename.split('.')[-1]
    image_bin = binary.read()

    is_inserted = selfieObj.insertSelfie(match_id, ext, user_id, image_bin)
    if is_inserted == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

##############
# Admin tool #
##############

@blueprint.route('/admin/login', methods=["GET", "POST"])
def admin_login():
    if request.method == "POST":
        loginObj = LoginAndSeat(g.db)
        parameters = lib.parse_request(request)
        user_name = parameters['user_name']
        password = parameters['password']

        is_admin = loginObj.adminLogin(user_name, password)
        if is_admin == True:
            session['is_loggedIn'] = True
            session['is_admin'] = True
            return make_response(json.jsonify(message="OK"), 200)
        else:
            return make_response(json.jsonify(message="Fail"), 403)

    elif request.method == "GET":
        return make_response(json.jsonify(
            is_loggedIn=session.get('is_loggedIn', False)
          , is_admin=session.get('is_admin', False)
            ), 200)

@lib.admin_required
@blueprint.route('/admin/today_match', methods=["GET", "POST"])
def today_match():
    matchManagerObj = MatchManager(g.db)
    if request.method == "POST":
        parameters = lib.parse_request(request)
        match_id = parameters['match_id']
        is_set = matchManagerObj.setTodayMatch(match_id)
        if is_set == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

    elif request.method == "GET":
        match_id, match_date, match_versus = matchManagerObj.getTodayMatch()
        return make_response(json.jsonify(
            id=match_id
          , date=match_date
          , versus=match_versus
          ), 200)

@lib.admin_required
@blueprint.route('/admin/match', methods=["GET", "POST"])
def matchInput():
    matchManagerObj = MatchManager(g.db)
    if request.method == "GET":
        columns, ret = matchManagerObj.checkMatch()
        result = lib.dbToDict(columns, ret)
        return make_response(json.jsonify(data=result), 200)

    elif request.method == "POST":
        parameters = lib.parse_request(request)
        when = parameters['date']
        versus = parameters['versus']

        is_input, new_match_id = matchManagerObj.addMatch(versus, when)
        if is_input == True:
            g.db.commit()
            return make_response(json.jsonify(match_id=new_match_id), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>', methods=["PUT", "DELETE"])
def matchUpdate(match_id):
    matchManagerObj = MatchManager(g.db)
    if request.method == "PUT":
        parameters = lib.parse_request(request)
        when = parameters['date']
        versus = parameters['versus']

        is_updated = matchManagerObj.updateMatch(match_id, versus, when)
        if is_updated == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

    elif request.method == "DELETE":
        is_deleted = matchManagerObj.deleteMatch(match_id)
        if is_deleted == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)


@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/download_attendee', methods=["GET"])
def downloadExcel(match_id):
    loginObj = LoginAndSeat(g.db)
    is_downloadable, result = loginObj.exportExcel(match_id)
    if is_downloadable == True:
        return send_file(io.BytesIO(result), attachment_filename='attendee_{0}.csv'.format(match_id), as_attachment=True)
    else:
        return make_response(json.jsonify(message="Fail"), 404)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/team_status', methods=["POST"])
def teamStatusUpdate(match_id):
    matchRecordObj = MatchRecord(g.db)

    parameters = lib.parse_request(request)
    image_url = parameters['image_url']
    is_input = matchRecordObj.setTeamRecord(match_id, image_url)
    if is_input == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/player_status', methods=["POST"])
def playerStatusUpdate(match_id):
    matchRecordObj = MatchRecord(g.db)

    parameters = lib.parse_request(request)
    image_url = parameters['image_url']
    is_input = matchRecordObj.setPlayerRecord(match_id, image_url)
    if is_input == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/intro_video', methods=["POST"])
def insertIntroVideo(match_id):
    introMovieObj = IntroMovie(g.db)

    parameters = lib.parse_request(request)
    video_url = parameters['video_url']

    is_input = introMovieObj.setMovieUrl(match_id, video_url)
    if is_input == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/coupon/template', methods=["GET", "POST"])
def insertCouponTemplate():
    couponObj = CouponManager(g.db)

    if request.method == "GET":
        columns, ret = couponObj.getCouponTemplates()
        data = lib.dbToDict(columns, ret)
        return make_response(json.jsonify(data=data), 200)

    elif request.method == "POST":
        parameters = lib.parse_request(request)
        name = parameters['name']
        description = parameters['description']

        is_input, coupon_id = couponObj.insertCouponTemplate(name, description)
        if is_input == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK", coupon_id=coupon_id), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/coupon/template/<coupon_id>', methods=["DELETE"])
def deleteCouponTemplate(coupon_id):
    couponObj = CouponManager(g.db)
    is_deleted = couponObj.deleteCouponTemplate(coupon_id)
    if is_deleted == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK", coupon_id=coupon_id), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/coupon', methods=["POST"])
def assignCoupon():
    couponObj = CouponManager(g.db)
    parameters = lib.parse_request(request)
    numbers = request.form.getlist('numbers[]')
    coupon_id = parameters['coupon_id']

    if type(numbers) != list:
        return make_response(json.jsonify(message="Fail"), 405)

    new_coupon_id = None
    for number in numbers:
        if number is not None and number != 'null' and number != u'' and number != '':
            can_get, user_id = lib.getUserIdByNumber(g.db, number)
            is_inserted, new_coupon_id = couponObj.assignCouponToUser(coupon_id, user_id)
            if is_inserted == False:
                g.db.rollback()
                return make_response(json.jsonify(message="Fail"), 405)

    g.db.commit()
    return make_response(json.jsonify(message="OK", coupon_id=new_coupon_id), 200)

@lib.admin_required
@blueprint.route('/admin/coupon/expired', methods=["DELETE"])
def deleteExpiredCoupon():
    couponObj = CouponManager(g.db)
    is_deleted = couponObj.deleteExpiredCoupon()
    if is_deleted == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)

    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/input_attendee', methods=["POST"])
def inputAttendee():
    pointObj = PointManager(g.db, is_production=IS_PRODUCTION)
    import csv

    csvFile_bin = request.files['csv_file']
    csvReader = csv.reader(csvFile_bin)

    for idx, row in enumerate(csvReader):
        if idx == 0:
            continue

        phoneNumber_raw = row[1]
        point_raw = row[3]

        phoneNumber = phoneNumber_raw.replace(' ', '').replace('-', '')
        pointTouching = point_raw.replace(' ', '').replace(',', '')

        is_input = pointObj.save_point(phoneNumber, pointTouching)
        if is_input == False:
            print phoneNumber
            print pointTouching
            return make_response(json.jsonify(message='Fail'), 405)

    return make_response(json.jsonify(message='OK'), 200)

@lib.admin_required
@blueprint.route('/admin/event/template', methods=["GET", "POST"])
def inputEventTemplates():
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    if request.method == "GET":
        columns, ret = eventObj.getEventTemplateList()
        data = lib.dbToDict(columns, ret)
        return make_response(json.jsonify(data=data), 200)

    elif request.method == "POST":
        parameters = lib.parse_request(request)

        name = parameters['name']
        description = parameters['description']
        category = parameters['category']
        point_needed = parameters['point_needed']

        is_inserted, event_id = eventObj.insertEventTemplate(name, category, point_needed, description)
        if is_inserted == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK", event_id=event_id), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/event/template/<int:event_id>', methods=["PUT", "DELETE"])
def updateEventTemplates(event_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    if request.method == "DELETE":
        is_deleted = eventObj.deleteEventTemplate(event_id)
        if is_deleted == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

    elif request.method == "PUT":
        parameters = lib.parse_request(request)

        name = parameters.get('name')
        description = parameters.get('description')
        category = parameters.get('category')
        point_needed = parameters.get('point_needed')

        data = {}
        if name != None:
            data['name'] = name
        if description != None:
            data['description'] = description
        if category != None:
            data['category'] = category
        if point_needed != None:
            data['point_needed'] = point_needed

        if len(data) == 0:
            data = None

        is_updated = eventObj.updateEventTemplate(event_id, **data)

        if is_updated == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/event', methods=["GET"])
def checkEventForToday(match_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    can_get, columns, ret = eventObj.getEventsOfMatch(match_id)
    data = lib.dbToDict(columns, ret)
    for idx, row in enumerate(data):
        columns_participants, ret_participants = eventObj.getJoinedEvents(match_id, row['id'])
        data_participants = lib.dbToDict(columns_participants, ret_participants)
        data[idx]['participants'] = data_participants

    return make_response(json.jsonify(data=data), 200)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/event/<int:event_id>/assign', methods=["POST"])
def assignEvent(match_id, event_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    is_deleted = eventObj.assignEventToMatch(match_id, event_id)
    if is_deleted == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/event/<int:event_id>/start', methods=["POST"])
def startEvent(match_id, event_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    is_started = eventObj.openEvent(match_id, event_id)
    if is_started == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/event/<int:event_id>/close', methods=["POST"])
def closeEvent(match_id, event_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    is_closed = eventObj.closeEvent(match_id, event_id)
    if is_closed == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/event/<int:event_id>/delete', methods=["DELETE"])
def deleteEvent(match_id, event_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    is_deleted = eventObj.unassignEventToMatch(match_id, event_id)
    if is_deleted == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/cancelRequestList', methods=["GET"])
def cancelRequestList(match_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    can_get, columns, result = eventObj.listCancelRequest(match_id)
    data = lib.dbToDict(columns, result)
    return make_response(json.jsonify(data=data), 200)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/event/<int:event_id>/user/<int:user_id>/confirmCancelEvent', methods=["DELETE"])
def confirmCancelEvent(match_id, event_id, user_id):
    eventObj = Event(g.db, is_production=IS_PRODUCTION)
    is_canceled = eventObj.confirmEventCancelRequest(match_id, event_id, user_id)
    if is_canceled == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/jukebox', methods=["GET"])
def getJukeboxList(match_id):
    jukeboxObj = Jukebox(g.db)
    columns, ret = jukeboxObj.checkJukebox(match_id)
    data = lib.dbToDict(columns, ret)
    return make_response(json.jsonify(data=data), 200)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/mvp_vote', methods=["GET"])
def getMVPVoteResult(match_id):
    mvpObj = MvpVote(g.db)
    columns, ret = mvpObj.checkVote(match_id)
    data = lib.dbToDict(columns, ret)
    return make_response(json.jsonify(data=data), 200)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/selfie', methods=["GET"])
def checkSelfieList(match_id):
    selfieObj = Selfie(g.db)
    column, ret = selfieObj.getSelfieList(match_id)
    data = lib.dbToDict(column, ret)
    for idx, row in enumerate(data):
        data[idx]['selfie_path'] = '/api/v1/admin/match/{0}/selfie/{1}'.format(match_id, row['user_id'])
    return make_response(json.jsonify(data=data), 200)

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/selfie/<user_id>', methods=["GET"])
def checkSelfieBin(match_id, user_id):
    selfieObj = Selfie(g.db)
    image_ext, image_bin = selfieObj.getSelfieBinary(match_id, user_id)
    return send_file(io.BytesIO(image_bin), attachment_filename='selfie.{0}'.format(image_ext))

@lib.admin_required
@blueprint.route('/admin/player', methods=["GET", "POST"])
def wholePlayer():
    playerManagerObj = PlayerManager(g.db)
    if request.method == "GET":
        columns, ret = playerManagerObj.getWholePlayerList()
        data = lib.dbToDict(columns, ret)
        for idx, row in enumerate(data):
            data[idx]['profile_pic_path'] = '/api/v1/admin/player/{0}/profile_pic'.format(row['id'])

        return make_response(json.jsonify(data=data), 200)

    elif request.method == "POST":
        parameters = lib.parse_request(request)
        position = parameters['position']
        number = parameters['number']
        name = parameters['name']
        height = parameters['height']
        weight = parameters['weight']
        
        is_inserted, new_player_id = playerManagerObj.insertPlayer(position, number, name, height, weight)
        if is_inserted == False:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

        if 'profile_pic' in request.files:
            binary = request.files['profile_pic']
            filename = binary.filename
            image_bin = binary.read()

            is_inserted_photo = playerManagerObj.setPlayerProfilePhoto(new_player_id, filename, image_bin)
            if is_inserted_photo == False:
                g.db.rollback()
                return make_response(json.jsonify(message="Fail"), 405)

        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)

@lib.admin_required
@blueprint.route('/admin/player/<int:player_id>', methods=["PUT", "DELETE"])
def managerPlayer(player_id):
    playerManagerObj = PlayerManager(g.db)
    if request.method == "PUT":
        parameters = lib.parse_request(request)

        data = {}
        if 'position' in parameters:
            data['position'] = parameters['position']
        if 'number' in parameters:
            data['number'] = parameters['number']
        if 'name' in parameters:
            data['name'] = parameters['name']
        if 'height' in parameters:
            data['height'] = parameters['height']
        if 'weight' in parameters:
            data['weight'] = parameters['weight']

        if len(data) > 0:
            is_info_updated = playerManagerObj.updatePlayer(player_id, **data)
            if is_info_updated == False:
                g.db.rollback()
                return make_response(json.jsonify(message="Fail"), 405)
        
        if 'profile_pic' in request.files:
            binary = request.files['profile_pic']
            filename = binary.filename
            image_bin = binary.read()
            is_photo_updated = playerManagerObj.setPlayerProfilePhoto(player_id, filename, image_bin)
            if is_photo_updated == False:
                g.db.rollback()
                return make_response(json.jsonify(message="Fail"), 405)

        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)

    elif request.method == "DELETE":
        is_deleted = playerManagerObj.deletePlayer(player_id)
        if is_deleted == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)

@lib.login_required
@blueprint.route('/admin/player/<int:player_id>/profile_pic', methods=["GET"])
def getPlayerProfilePic_admin(player_id):
    playerManagerObj = PlayerManager(g.db)
    can_get_list, photo_ext, photo_bin = playerManagerObj.getPlayerProfilePhoto(player_id)
    if can_get_list == False:
        return make_response(json.jsonify(message="Failure"), 400)

    return send_file(io.BytesIO(photo_bin), attachment_filename='profile.{0}'.format(photo_ext))

@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/players', methods=["GET", "POST"])
def assignPlayerToMatch(match_id):
    playerManagerObj = PlayerManager(g.db)
    if request.method == "GET":
        can_get, columns, ret = playerManagerObj.getLineUpForSpecificMatch(match_id)
        data = lib.dbToDict(columns, ret)
        for idx, row in enumerate(data):
            data[idx]['profile_pic_path'] = '/api/v1/admin/player/{0}/profile_pic'.format(row['id'])

        return make_response(json.jsonify(data=data), 200)

    elif request.method == "POST":
        parameters = lib.parse_request(request)
        player_id = parameters['player_id']
        is_assigned = playerManagerObj.setLineUpForSpecificMatch(match_id, player_id)
        if is_assigned == True:
            g.db.commit()
            return make_response(json.jsonify(message="OK"), 200)
        else:
            g.db.rollback()
            return make_response(json.jsonify(message="Fail"), 405)
    
@lib.admin_required
@blueprint.route('/admin/match/<int:match_id>/players/<int:player_id>', methods=["DELETE"])
def unassignPlayerToMatch(match_id, player_id):
    playerManagerObj = PlayerManager(g.db)
    is_deleted = playerManagerObj.deleteLineUpForSpecificMatch(match_id, player_id)
    if is_deleted == True:
        g.db.commit()
        return make_response(json.jsonify(message="OK"), 200)
    else:
        g.db.rollback()
        return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/getAllTokenPerMatch', methods=["GET"])
def getAllTokenPerMatch():
    if request.method == "GET":
        loginManObj = LoginAndSeat(g.db)
        match_id = request.args.get('match_id', 1)
        can_get, result = loginManObj.providePushTokensPerMatch(match_id)
        if can_get == True:
            return make_response(json.jsonify(data=result), 200)
        else:
            return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/getUserToken', methods=["GET"])
def getTokenPerUser():
    if request.method == "GET":
        loginManObj = LoginAndSeat(g.db)
        phone_no = request.args.get('user_phoneNumber', '01012345678')
        is_ok, user_id = lib.getUserIdByNumber(g.db, phone_no)
        can_get, device_category, token = loginManObj.providePushTokensPerUser(user_id)
        if can_get == True:
            return make_response(json.jsonify(device_category=device_category, token=token), 200)
        else:
            return make_response(json.jsonify(message="Fail"), 405)

@lib.admin_required
@blueprint.route('/admin/searchUser', methods=["GET"])
def searchUser():
    if request.method == "GET":
        loginManObj = LoginAndSeat(g.db)
        _, match_id = lib.getTodayMatchId(g.db)
        columns = None
        value = None

        for key in request.args:
            if key == 'name':
                name = request.args.get('name')
                _, columns, value = loginManObj.findUserByName(name, match_id)
                break

            if key == 'phone_number':
                phoneNumber = request.args.get('phone_number')
                _, columns, value = loginManObj.findUserByPhone(phoneNumber, match_id)
                break

        data = lib.dbToDict(columns, value)

        return make_response(json.jsonify(data=data), 200)
