# -*- coding: utf-8 -*-

import requests

__all__ = ['TOUCHING_API_URL_TEST', 'Touching']
__all__ = ['TOUCHING_API_URL_PROD', 'Touching']

TOUCHING_API_URL_TEST = 'http://api-test.touchinga.com'
TOUCHING_API_URL_PROD = 'http://www.touchingpos.com'

class Touching(object):
    def __init__(self, token, is_production=False):
        self.token = token

        if is_production == True:
            self.url = TOUCHING_API_URL_PROD
        else:
            self.url = TOUCHING_API_URL_TEST

        requests_session = requests.Session()
        requests_adapters = requests.adapters.HTTPAdapter(max_retries=3)
        requests_session.mount('http://', requests_adapters)
        self.requests_session = requests_session

    class ResponseError(Exception):
        def __init__(self, code=None, message=None):
            self.code = code
            self.message = message

    @staticmethod
    def get_response(response):
        result = response.json()
        return result

    def _get(self, url, payload=None):
        headers = self.get_headers()
        response = self.requests_session.get(url, headers=headers, params=payload)
        return self.get_response(response)

    def _post(self, url, payload=None):
        headers = self.get_headers()
        response = self.requests_session.post(url, headers=headers, data=payload)
        return self.get_response(response)

    def _delete(self, url, payload=None):
        headers = self.get_headers()
        response = self.requests_session.delete(url, headers=headers, params=payload)
        return self.get_response(response)

    def get_headers(self):
        return {'Authorization': "Bearer {}".format(self.token)}

    def check_userInfo(self, tel):
        url = "{}/api/v1/milecards".format(self.url)
        payload = {"tel": tel}
        return self._get(url, payload=payload)

    def check_point(self, tel):
        url = "{}/api/v1/milelogs".format(self.url)
        payload = {"tel": tel}
        return self._get(url, payload=payload)

    def save_point(self, tel, point, use_coupons=None):
        url = "{}/api/v1/savings".format(self.url)
        payload = {"tel": tel, "new_mile": point, "use_coupons[]": ','.join(use_coupons) if use_coupons is not None else ""}
        print "    Request is sent to touching. {}/{}".format(tel, point)
        return self._post(url, payload=payload)

    def use_point(self, tel, point, use_coupons=None):
        url = "{}/api/v1/savings".format(self.url)
        payload = {"tel": tel, "new_mile": -1 * point, "use_coupons[]": ','.join(use_coupons) if use_coupons is not None else ""}
        print "    Request is sent to touching. {}/{}".format(tel, point)
        return self._post(url, payload=payload)

    def delete_point(self, touching_save_id):
        url = "{}/api/v1/milelogs/{}".format(self.url, touching_save_id)
        return self._delete(url)
