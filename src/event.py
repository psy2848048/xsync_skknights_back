# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback
from touching import Touching

class NotOpenedException(Exception):
    def __init__(self, error_str):
        self.message = error_str

class Event:
    def __init__(self, conn, is_production=False):
        self.conn = conn
        self.touching = Touching('nGyQkfFYzG_xNXV60U89odyp3R5xVK1b', is_production=is_production)

    def _checkOpen(self, match_id, event_id):
        cursor = self.conn.cursor()
        query = """
            SELECT is_open
            FROM XSYNC.F_EVENT_PER_MATCH
            WHERE
                    match_id = %s
                AND event_id = %s
        """
        cursor.execute(query, (match_id, event_id, ))
        ret = cursor.fetchone()
        if ret is None or len(ret) == 0:
            return None

        return ret[0]

    def _getPoint(self, event_id):
        cursor = self.conn.cursor()
        query = """
            SELECT point_in_use
            FROM XSYNC.D_EVENTS
            WHERE id = %s
        """
        cursor.execute(query, (event_id, ))
        ret = cursor.fetchone()
        if ret is None or len(ret) == 0:
            return None

        return ret[0]

    def _getCurrentNumberOfParticipant(self, match_id, event_id):
        cursor = self.conn.cursor()
        query = """
            SELECT count(*)
            FROM XSYNC.F_EVENT_PARTICIPANTS
            WHERE match_id = %s
              AND event_id = %s
              AND (
                     (cancel_request = true AND cancel_confirm = false)
                  OR (cancel_request is null AND cancel_confirm is null)
                  )
        """
        cursor.execute(query, (match_id, event_id, ))
        ret = cursor.fetchone()
        if ret is None or len(ret) == 0:
            return None

        return ret[0]

    def _getDefinedParticipant(self, event_id):
        cursor = self.conn.cursor()
        query = """
            SELECT choice_method
            FROM XSYNC.D_EVENTS
            WHERE id = %s
        """
        cursor.execute(query, (event_id, ))
        ret = cursor.fetchone()
        if ret is None or len(ret) == 0:
            return None

        number = 0
        if '/' in ret[0]:
            print "    First come, first serve event"
            number = int(ret[0].split('/')[1])
        else:
            print "    Whole"
            number = 10000

        return number

    def getJoinedEvents(self, match_id, event_id):
        cursor = self.conn.cursor()
        query = """
            SELECT
                users.id as id,
                users.name as name,
                users.phoneNumber as phone_number
            FROM XSYNC.F_EVENT_PARTICIPANTS participant
            JOIN XSYNC.D_USERS users
              ON participant.user_id = users.id
            WHERE
                    participant.match_id = %s
                AND participant.event_id = %s
                AND (
                       (cancel_request = true AND cancel_confirm = false)
                    OR (cancel_request is null AND cancel_confirm is null)
                    )
        """
        cursor.execute(query, (match_id, event_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return columns, ret

    def getJoinedEventsOfUser(self, match_id, user_id):
        cursor = self.conn.cursor()
        query = """
            SELECT
                events.id as id,
                events.event_name as name,
                events.choice_method as category,
                events.point_in_use as point_needed,
                events.description
            FROM XSYNC.F_EVENT_PARTICIPANTS participant
            JOIN XSYNC.D_EVENTS events
              ON participant.event_id = events.id
            WHERE
                    participant.match_id = %s
                AND participant.user_id = %s
                AND (
                       (participant.cancel_request = true AND participant.cancel_confirm = false)
                    OR (participant.cancel_request is null AND participant.cancel_confirm is null)
                    )
        """
        cursor.execute(query, (match_id, user_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return True, columns, ret

    def getEventTemplateList(self):
        cursor = self.conn.cursor()
        query = """
            SELECT id,
                   event_name as name,
                   choice_method as category,
                   point_in_use as point_needed,
                   description
            FROM XSYNC.D_EVENTS
            ORDER BY id
        """
        cursor.execute(query)
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return columns, ret

    def insertEventTemplate(self, event_name, choice_method, point_in_use, description):
        cursor = self.conn.cursor()

        new_event_id = lib.get_new_id(self.conn, "D_EVENTS")
        query = """
            INSERT INTO XSYNC.D_EVENTS
                (id, event_name, choice_method, point_in_use, description)
            VALUES
                (%s, %s, %s, %s, %s)
        """
        try:
            cursor.execute(query, (new_event_id, event_name, choice_method, point_in_use, description, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, None

        return True, new_event_id

    def updateEventTemplate(self, event_id, **kwargs):
        cursor = self.conn.cursor()

        keys = []
        values = []
        for key, value in kwargs.iteritems():
            keys.append("{0}=%s".format(key))
            values.append(value)

        query = """
            UPDATE XSYNC.D_EVENTS
            SET {0}
            WHERE id = %s
        """.format( ', '.join(keys) )
        values.append(event_id)
        try:
            cursor.execute(query, values)

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def deleteEventTemplate(self, event_id):
        cursor = self.conn.cursor()

        query = """
            DELETE FROM XSYNC.D_EVENTS
            WHERE id = %s
        """

        try:
            cursor.execute(query, (event_id, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def getEventsOfMatch(self, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT
                events.id,
                events.event_name as name,
                events.choice_method as category,
                events.point_in_use as point_needed,
                events.description as description,
                eventpermatch.is_open
            FROM XSYNC.F_EVENT_PER_MATCH eventpermatch
            JOIN XSYNC.D_EVENTS events
              ON eventpermatch.event_id = events.id
            WHERE eventpermatch.match_id = %s
        """
        cursor.execute(query, (match_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return True, columns, ret

    def assignEventToMatch(self, match_id, event_id):
        cursor = self.conn.cursor()
        query = """
            INSERT INTO XSYNC.F_EVENT_PER_MATCH
                (match_id, event_id, is_open)
            VALUES
                (%s, %s, false)
        """
        try:
            cursor.execute(query, (match_id, event_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def unassignEventToMatch(self, match_id, event_id):
        cursor = self.conn.cursor()
        query = """
            DELETE FROM XSYNC.F_EVENT_PER_MATCH
            WHERE match_id = %s AND event_id = %s
        """
        try:
            cursor.execute(query, (match_id, event_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def openEvent(self, match_id, event_id):
        cursor = self.conn.cursor()
        query = """
            UPDATE XSYNC.F_EVENT_PER_MATCH
            SET is_open = true
            WHERE match_id = %s AND event_id = %s
        """
        try:
            cursor.execute(query, (match_id, event_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def closeEvent(self, match_id, event_id):
        cursor = self.conn.cursor()
        query = """
            UPDATE XSYNC.F_EVENT_PER_MATCH
            SET is_open = false
            WHERE match_id = %s AND event_id = %s
        """
        try:
            cursor.execute(query, (match_id, event_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def joinEvent(self, match_id, event_id, user_id):
        cursor = self.conn.cursor()
        is_open = self._checkOpen(match_id, event_id)
        if is_open == False:
            raise NotOpenedException("Closed event")
        query_participationCheck = """
            SELECT count(*)
            FROM XSYNC.F_EVENT_PARTICIPANTS
            WHERE match_id = %s
              AND event_id = %s
              AND user_id = %s
              AND cancel_request is null
              AND cancel_confirm is null
        """
        cursor.execute(query_participationCheck, (match_id, event_id, user_id,))
        count = cursor.fetchone()[0]
        if count > 0:
            return 2

        query_calcelRequestCheck = """
            SELECT count(*)
            FROM XSYNC.F_EVENT_PARTICIPANTS
            WHERE match_id = %s
              AND event_id = %s
              AND user_id = %s
              AND cancel_request = true
              AND cancel_confirm = false
        """
        cursor.execute(query_calcelRequestCheck, (match_id, event_id, user_id,))
        count = cursor.fetchone()[0]
        if count > 0:
            return 4

        can_get, user_number = lib.getNumberByUserId(self.conn, user_id)
        
        result = self.touching.check_userInfo(user_number)
        current_point = result['milecard'].get('current_mile', 0) if result.get('milecard') is not None and 'current_mile' in result['milecard'] else 0
        point_needed = self._getPoint(event_id)

        if current_point < point_needed:
            return 1

        max_participant = self._getDefinedParticipant(event_id)
        cur_participant = self._getCurrentNumberOfParticipant(match_id, event_id)

        if max_participant <= cur_participant:
            return 3

        # Get the amount of point of the event
        cursor.execute("""SELECT point_in_use FROM XSYNC.D_EVENTS WHERE id = %s""", (event_id, ))
        point_tmp = cursor.fetchone()
        if point_tmp is None or len(point_tmp) == 0:
            return 4 # Not exist event

        point = point_tmp[0]
        return_data = self.touching.use_point(user_number, point)
        milelog_id = int(return_data['milelog'].get('id', None)) if return_data.get('milelog') is not None and 'id' in return_data['milelog'] else None

        query = """
            INSERT INTO XSYNC.F_EVENT_PARTICIPANTS
                (match_id, event_id, user_id, milelog_id)
            VALUES
                (%s, %s, %s, %s)
        """
        try:
            cursor.execute(query, (match_id, event_id, user_id, milelog_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return 2

        return 0

    def exitEvent(self, match_id, event_id, user_id):
        cursor = self.conn.cursor()
        query = """
            DELETE FROM XSYNC.F_EVENT_PARTICIPANTS
            WHERE 
                  match_id = %s
              AND event_id = %s
              AND user_id = %s
        """
        try:
            cursor.execute(query, (match_id, event_id, user_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def requestCancel(self, match_id, event_id, user_id, description):
        cursor = self.conn.cursor()
        query_participationCheck = """
            SELECT count(*) FROM XSYNC.F_EVENT_PARTICIPANTS
            WHERE match_id = %s
              AND event_id = %s
              AND user_id = %s
              AND cancel_request is null
              AND cancel_confirm is null
        """
        try:
            cursor.execute(query_participationCheck, (match_id, event_id, user_id, ))
        except Exception:
            traceback.print_exc()
            return 1

        count = cursor.fetchone()[0]
        if count == 0:
            return 2

        query = """
            UPDATE XSYNC.F_EVENT_PARTICIPANTS
              SET cancel_request = true,
                  cancel_confirm = false,
                  cancel_description = %s
            WHERE match_id = %s
              AND event_id = %s
              AND user_id = %s
              AND cancel_request is null
              AND cancel_confirm is null
        """
        try:
            cursor.execute(query, (description, match_id, event_id, user_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return 1

        return 0

    def listCancelRequest(self, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT users.phoneNumber,
                   users.name,
                   users.id user_id,
                   events.event_name,
                   events.id,
                   participants.cancel_description
            FROM XSYNC.F_EVENT_PARTICIPANTS participants
            LEFT OUTER JOIN XSYNC.D_USERS users
              ON participants.user_id = users.id
            LEFT OUTER JOIN XSYNC.D_EVENTS events
              ON participants.event_id = events.id
            WHERE participants.match_id = %s
              AND participants.cancel_request = true
              AND participants.cancel_confirm = false
        """
        try:
            cursor.execute(query, (match_id, ))
        except Exception:
            traceback.print_exc()
            return False, None, None

        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return True, columns, ret

    def confirmEventCancelRequest(self, match_id, event_id, user_id):
        cursor = self.conn.cursor()

        query_saveId = """
            SELECT milelog_id
            FROM XSYNC.F_EVENT_PARTICIPANTS
            WHERE match_id = %s
              AND event_id = %s
              AND user_id = %s
              AND (
                     (cancel_request = true AND cancel_confirm = false)
                  OR (cancel_request is null AND cancel_confirm is null)
                  )
            ORDER BY milelog_id DESC
            LIMIT 1
        """
        cursor.execute(query_saveId, (match_id, event_id, user_id, ))
        milelog_id = cursor.fetchone()[0]
        try:
            data = self.touching.delete_point(milelog_id)
            print "    Event canceled! EventID: {} / UserID: {}".format(event_id, user_id)
        except Exception:
            traceback.print_exc()
            return False

        query = """
            UPDATE XSYNC.F_EVENT_PARTICIPANTS
              SET cancel_confirm = true
                , cancel_request = true
            WHERE match_id = %s
              AND event_id = %s
              AND user_id = %s
              AND (
                     (cancel_request = true AND cancel_confirm = false)
                  OR (cancel_request is null AND cancel_confirm is null)
                  )
        """
        try:
            cursor.execute(query, (match_id, event_id, user_id, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True
