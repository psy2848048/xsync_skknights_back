# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback


class Selfie:
    def __init__(self, conn):
        self.conn = conn

    def insertSelfie(self, match_id, ext, user_id, image_bin):
        cursor = self.conn.cursor()

        query = """
            UPDATE XSYNC.F_SELFIE
                SET image_bin = %s,
                    image_ext = %s,
                    registered_time = CURRENT_TIMESTAMP
            WHERE   
                    match_id = %s
                AND user_id = %s
        """
        query_insert = """
            INSERT INTO XSYNC.F_SELFIE
                (match_id, user_id, image_bin, image_ext, registered_time)
            SELECT %s, %s, %s, %s, CURRENT_TIMESTAMP
                WHERE NOT EXISTS (SELECT 1 FROM XSYNC.F_SELFIE WHERE match_id = %s AND user_id = %s )
        """
        try:
            cursor.execute(query, (bytearray(image_bin), ext, match_id, user_id, ))
            cursor.execute(query_insert, (match_id, user_id, bytearray(image_bin), ext, match_id, user_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def getSelfieList(self, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT 
                selfie.registered_time registered_time,
                users.id as user_id,
                users.phoneNumber as phoneNumber,
                users.name as name,
                seat.section as section,
                seat.row as row,
                seat.seat as seat
            FROM XSYNC.F_SELFIE selfie
            JOIN XSYNC.D_USERS users
              ON users.id = selfie.user_id
            LEFT OUTER JOIN XSYNC.F_SEAT_TABLE_PER_MATCH seat
              ON users.id = seat.user_id
            WHERE selfie.match_id = %s AND seat.match_id = %s
            ORDER BY selfie.registered_time DESC LIMIT 100
        """
        cursor.execute(query, (match_id, match_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        res = cursor.fetchall()
        return columns, res

    def getSelfieBinary(self, match_id, user_id):
        cursor = self.conn.cursor()
        query = """
            SELECT image_ext, image_bin
            FROM XSYNC.F_SELFIE
            WHERE   match_id = %s
                AND user_id = %s
        """
        cursor.execute(query, (match_id, user_id, ))
        ret = cursor.fetchone()
        image_ext = ret[0]
        image_bin = ret[1]
        return image_ext, image_bin
