# -*- coding: utf-8 -*-

from flask import Flask, session
from flask_session import Session
from flask_cors import CORS 
from datetime import timedelta

import bp_ver1

VERSION = '1.1'
DEBUG = True
SECRET_KEY = 'eufiqwgeiucgbq3uwefvyy387edertwggbvacdewrgf'

PERMANENT_SESSION_LIFETIME = timedelta(days=15)
SESSION_TYPE = 'redis'
SESSION_COOKIE_NAME = "SKKnightsCookie"

# Flask-Session
app = Flask(__name__)
app.config.from_object(__name__)
app.register_blueprint(bp_ver1.blueprint, url_prefix='/api/v1')

# CORS
cors = CORS(app, resources={r"/*": {"origins": "*", "supports_credentials": "true"}})
# Flask-Session
Session(app)

if __name__ == "__main__":
    from gevent.wsgi import WSGIServer
    http_server = WSGIServer(('0.0.0.0', 5000), app)
    http_server.serve_forever()
    #app.run(host='0.0.0.0', port=5000, threaded=True)
