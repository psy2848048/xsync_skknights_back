# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback


class MatchManager:
    def __init__(self, conn):
        self.conn = conn

    def checkMatch(self):
        cursor = self.conn.cursor()
        query = """
            SELECT id, match_time as date, versus
            FROM XSYNC.D_MATCHES
            ORDER BY match_time DESC
        """
        cursor.execute(query)
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()

        return columns, ret

    def addMatch(self, versus, match_time):
        cursor = self.conn.cursor()

        new_match_id = lib.get_new_id(self.conn, "D_MATCHES")
        query = """
            INSERT INTO XSYNC.D_MATCHES
                (id, versus, match_time)
            VALUES
                (%s, %s, %s)
        """
        try:
            cursor.execute(query, (new_match_id, versus, match_time, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, None

        return True, new_match_id

    def updateMatch(self, match_id, versus, match_time):
        cursor = self.conn.cursor()

        query = """
            UPDATE XSYNC.D_MATCHES
               SET versus = %s,
                   when = %s
            WHERE id = %s
        """
        try:
            cursor.execute(query, (versus, match_time, match_id, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def deleteMatch(self, match_id):
        cursor = self.conn.cursor()

        query = """
            DELETE FROM XSYNC.D_MATCHES
            WHERE id = %s
        """
        try:
            cursor.execute(query, (match_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def setTodayMatch(self, match_id):
        cursor = self.conn.cursor()
        query_delete = """
            DELETE FROM XSYNC.TODAY_MATCH
        """
        query_insert = """
            INSERT INTO XSYNC.TODAY_MATCH
                (match_id)
            VALUES (%s)
        """
        try:
            cursor.execute(query_delete)
            cursor.execute(query_insert, (match_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def getTodayMatch(self):
        cursor = self.conn.cursor()
        query = """
            SELECT id, match_time, versus
            FROM XSYNC.D_MATCHES
            WHERE id = (SELECT match_id FROM XSYNC.TODAY_MATCH LIMIT 1)
        """
        cursor.execute(query)
        ret = cursor.fetchone()
        return ret[0], ret[1], ret[2]
