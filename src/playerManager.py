# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback


class PlayerManager:
    def __init__(self, conn):
        self.conn = conn

    def insertPlayer(self, position, back_number, name, height, weight):
        cursor = self.conn.cursor()

        new_player_id = lib.get_new_id(self.conn, "D_PLAYERS")

        query = """
            INSERT INTO XSYNC.D_PLAYERS
                (id, position, number, name, height, weight)
            VALUES
                (%s, %s, %s, %s, %s, %s)
        """
        try:
            cursor.execute(query, (new_player_id, position, back_number, name, height, weight, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, None

        return True, new_player_id

    def updatePlayer(self, player_id, **kwargs):
        cursor = self.conn.cursor()

        keys = []
        values = []
        for key, value in kwargs.iteritems():
            keys.append("{0}=%s".format(key))
            values.append(value)

        query = """
            UPDATE XSYNC.D_PLAYERS
            SET {0}
            WHERE id = %s
        """.format( ','.join(keys) )
        values.append(player_id)
        try:
            cursor.execute(query, values)

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def deletePlayer(self, player_id):
        cursor = self.conn.cursor()
        query = """
            DELETE FROM XSYNC.D_PLAYERS
            WHERE id = %s
        """
        try:
            cursor.execute(query, (player_id, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def getWholePlayerList(self):
        cursor = self.conn.cursor()
        query = """
            SELECT 
                id, position, number, name, height, weight
            FROM XSYNC.D_PLAYERS
            ORDER BY number ASC
        """
        cursor.execute(query)
        columns = [ desc[0] for desc in cursor.description ]
        res = cursor.fetchall()

        return columns, res

    def getSpecificPlayerList(self, player_id):
        cursor = self.conn.cursor()
        query = """
            SELECT 
                id, position, number, name, height, weight
            FROM XSYNC.D_PLAYERS
            WHERE id = %s
            ORDER BY number ASC
        """
        cursor.execute(query, (player_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        res = cursor.fetchone()

        return columns, res

    def setPlayerProfilePhoto(self, player_id, filename, image_bin):
        cursor = self.conn.cursor()
        ext = filename.split('.')[-1]
        query = """
            UPDATE XSYNC.D_PLAYERS
            SET   profile_pic_ext = %s
                , profile_pic_bin = %s
            WHERE id = %s
        """
        try:
            cursor.execute(query, (ext, bytearray(image_bin), player_id, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def getPlayerProfilePhoto(self, player_id):
        cursor = self.conn.cursor()
        query = """
            SELECT profile_pic_ext, profile_pic_bin
            FROM XSYNC.D_PLAYERS
            WHERE id = %s
        """
        cursor.execute(query, (player_id, ))
        ret = cursor.fetchone()
        if ret is None or len(ret) == 0:
            return False, None, None

        return True, ret[0], ret[1]

    def getLineUpForSpecificMatch(self, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT 
              players.id, 
              players.position, 
              players.number, 
              players.name, 
              players.height, 
              players.weight
            FROM XSYNC.F_PLAYERS_PER_MATCH match
            JOIN XSYNC.D_PLAYERS players
              ON match.player_id = players.id
            WHERE match.match_id = %s
            ORDER BY players.number ASC
        """
        cursor.execute(query, (match_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()

        if ret is None or len(ret) == 0:
            return  False, None, None

        return True, columns, ret

    def setLineUpForSpecificMatch(self, match_id, player_id):
        cursor = self.conn.cursor()
        query = """
            INSERT INTO XSYNC.F_PLAYERS_PER_MATCH
                (match_id, player_id)
            VALUES
                (%s, %s)
        """
        try:
            cursor.execute(query, (match_id, player_id, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def deleteLineUpForSpecificMatch(self, match_id, player_id):
        cursor = self.conn.cursor()
        query = """
            DELETE FROM XSYNC.F_PLAYERS_PER_MATCH
            WHERE match_id = %s 
              AND player_id = %s
        """
        try:
            cursor.execute(query, (match_id, player_id, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

