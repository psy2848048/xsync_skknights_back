# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback


class Jukebox:
    def __init__(self, conn):
        self.conn = conn

    def requestSong(self, match_id, name, description, song):
        cursor = self.conn.cursor()
        query = """
            INSERT INTO XSYNC.F_JUKEBOX
                (match_id, name, description, song_and_singer, registered_time)
            VALUES
                (%s, %s, %s, %s, CURRENT_TIMESTAMP)
        """
        try:
            cursor.execute(query, (match_id, name, description, song, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def checkJukebox(self, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT name, description, song_and_singer as song
            FROM XSYNC.F_JUKEBOX
            WHERE match_id = %s
            ORDER BY registered_time DESC
        """
        cursor.execute(query, (match_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return columns, ret
