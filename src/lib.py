# -*- coding: utf-8 -*-
from collections import OrderedDict
from flask import make_response
from functools import wraps

def get_new_id(conn, table):
    """  
    테이블에서 사용하는 sequece에서 새 ID를 따줌.
    이 기능을 사용하려면 seqeunce 이름을 SEQ_<table_name>으로 해야 함 (ex CICERON.SEQ_F_REQUESTS)
    """
    cursor = conn.cursor()
    cursor.execute("SELECT nextval('XSYNC.SEQ_%s') " % table)
    current_id_list = cursor.fetchone()
    return current_id_list[0]

def getUserId(conn, name, phoneNumber):
    """
    이름과 폰번호로 DB에서 user_id 찾기.
    Touching과는 상관없음
    """
    cursor = conn.cursor()

    query = """
        SELECT id
        FROM XSYNC.D_USERS
        WHERE phoneNumber = %s
    """
    cursor.execute(query, (phoneNumber, ))
    res = cursor.fetchone()

    if res is None or len(res) == 0:
        return False, None

    return True, res[0]

def getUserIdByNumber(conn, phoneNumber):
    """
    폰번호로 DB에서 user_id 찾기.
    Touching과는 상관없음
    """
    cursor = conn.cursor()

    query = """
        SELECT id
        FROM XSYNC.D_USERS
        WHERE phoneNumber = %s
    """
    cursor.execute(query, (phoneNumber, ))
    res = cursor.fetchone()

    if res is None or len(res) == 0:
        return False, None

    return True, res[0]

def getNumberByUserId(conn, user_id):
    cursor = conn.cursor()

    query = """
        SELECT phoneNumber
        FROM XSYNC.D_USERS
        WHERE id = %s
    """
    cursor.execute(query, (user_id, ))
    res = cursor.fetchone()

    if res is None or len(res) == 0:
        return False, None

    return True, res[0]

def getTodayMatchId(conn):
    """
    오늘의 경기 ID 가져오기
    """

    cursor = conn.cursor()

    query = """
        SELECT MAX(match_id) FROM XSYNC.TODAY_MATCH GROUP BY match_id ORDER BY match_id desc LIMIT 1
    """
    cursor.execute(query)
    res = cursor.fetchone()

    if res is None or len(res) == 0:
        return False, None

    return True, res[0]

def parse_request(req):
    """  
    application/json, application/x-wwwurlencode, multipart/form-data 같은 다양한 형식에서도
    POST request를 python dict 형태로 나타낼 수 있게 만들어주는 라이브러리이다.
    """
    if len(req.form) == 0 and len(req.files) == 0:
        parameter_list = req.get_json()
        if parameter_list != None:
            result = dict()

            for key, value in parameter_list.iteritems():
                result[key] = value

            return result
     
        else:
            return dict()

    else:
        if len(req.form) != 0:
            result = dict()
            for key, value in req.form.iteritems():
                result[key] = value

            return result

        else:
            return dict()

def dbToDict(columns, ret):
    result = []
    if ret is None or len(ret) == 0:
        return result

    for row in ret:
        item = { columns[idx]:col for idx, col in enumerate(row) }
        result.append(item)

    return result

def login_required(f):
    """  
    로그인이 되어 있는지 세션 설정값을 본다.
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'is_loggedIn' in session:
            return f(*args, **kwargs)
        else:
            return make_response(json.jsonify(
                       status_code = 401, 
                       message = "Login required"
               ), 403) 

def admin_required(f):
    """  
    로그인이 되어 있는지 세션 설정값을 본다.
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('is_loggedIn', False) == True and session.get('is_admin', False) == True:
            print session.get('is_loggedIn', False)
            print session.get('is_admin', False)
            return f(*args, **kwargs)
        else:
            return make_response(json.jsonify(
                       status_code = 403, 
                       message = "Login required"
               ), 403) 
