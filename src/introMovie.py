# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback


class IntroMovie:
    def __init__(self, conn):
        self.conn = conn

    def setMovieUrl(self, match_id, movie_url):
        cursor = self.conn.cursor()
        query = """
           UPDATE XSYNC.F_INTROMOVIE
             SET movie_url = %s
           WHERE match_id = %s
        """
        query_insert = """
            INSERT INTO XSYNC.F_INTROMOVIE
                (match_id, movie_url)
            SELECT %s, %s
                WHERE NOT EXISTS (SELECT 1 FROM XSYNC.F_INTROMOVIE WHERE match_id = %s)
        """
        try:
            cursor.execute(query, (movie_url, match_id, ))
            cursor.execute(query_insert, (match_id, movie_url, match_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def getMovieUrl(self, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT movie_url
              FROM XSYNC.F_INTROMOVIE
            WHERE match_id = %s
        """
        cursor.execute(query, (match_id, ))
        ret = cursor.fetchone()
        if ret is None or len(ret) == 0:
            return False, None

        return True, ret[0]
