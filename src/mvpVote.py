# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback

class MvpVote:
    def __init__(self, conn):
        self.conn = conn

    def _checkDoubleVote(self, match_id, user_id):
        cursor = self.conn.cursor()
        query = """
            SELECT count(*)
            FROM XSYNC.F_MVP_VOTED_USERS
            WHERE
                  match_id = %s
              AND user_id = %s
              AND is_voted = true
        """
        cursor.execute(query, (match_id, user_id, ))
        count = cursor.fetchone()[0]

        if count > 0:
            return False
        else:
            return True

    def _markAsVoted(self, match_id, user_id):
        cursor = self.conn.cursor()
        query = """
            INSERT INTO XSYNC.F_MVP_VOTED_USERS
                (match_id, user_id, is_voted)
            VALUES
                (%s, %s, true)
        """
        try:
            cursor.execute(query, (match_id, user_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def voteMvp(self, user_id, match_id, player_id):
        cursor = self.conn.cursor()

        is_voted = self._checkDoubleVote(match_id, user_id)

        is_marked_as_voted = self._markAsVoted(match_id, user_id)
        if is_marked_as_voted == False:
            return False, 1

        query = """
            INSERT INTO XSYNC.F_MVP_VOTE
                (match_id, player_id)
            VALUES
                (%s, %s)
        """
        try:
            cursor.execute(query, (match_id, player_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, 2

        return True, 0

    def checkVote(self, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT
                players.id,
                players.name,
                players.number,
                counter.rate
            FROM (SELECT player_id, count(*) as rate FROM XSYNC.F_MVP_VOTE WHERE match_id = %s GROUP BY player_id) counter
            JOIN XSYNC.D_PLAYERS players
              ON counter.player_id = players.id
            ORDER BY counter.rate DESC
        """
        cursor.execute(query, (match_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return columns, ret
