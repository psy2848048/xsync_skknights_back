# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback


class MatchRecord:
    def __init__(self, conn):
        self.conn = conn

    def getTeamRecord(self, match_id):
        cursor = self.conn.cursor()

        query = """
            SELECT record_url
            FROM XSYNC.F_MATCH_RECORDS
            WHERE match_id = %s
        """
        cursor.execute(query, (match_id, ))
        ret = cursor.fetchone()
        if ret is None or len(ret) == 0:
            return False, None

        return True, ret[0]

    def getPlayerRecord(self, match_id):
        cursor = self.conn.cursor()

        query = """
            SELECT record_url
            FROM XSYNC.F_PLAYER_RECORDS
            WHERE match_id = %s
        """
        cursor.execute(query, (match_id, ))
        ret = cursor.fetchone()
        if ret is None or len(ret) == 0:
            return False, None

        return True, ret[0]

    def setTeamRecord(self, match_id, record_url):
        cursor = self.conn.cursor()

        query = """
            UPDATE XSYNC.F_MATCH_RECORDS
            SET record_url = %s
            WHERE match_id = %s
        """
        query_insert = """
            INSERT INTO XSYNC.F_MATCH_RECORDS
                (match_id, record_url)
            SELECT %s, %s
                WHERE NOT EXISTS (SELECT 1 FROM XSYNC.F_MATCH_RECORDS WHERE match_id = %s)
        """
        try:
            cursor.execute(query, (record_url, match_id, ))
            cursor.execute(query_insert, (match_id, record_url, match_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def setPlayerRecord(self, match_id, record_url):
        cursor = self.conn.cursor()

        query = """
            UPDATE XSYNC.F_PLAYER_RECORDS
            SET record_url = %s
            WHERE match_id = %s
        """
        query_insert = """
            INSERT INTO XSYNC.F_PLAYER_RECORDS
                (match_id, record_url)
            SELECT %s, %s
                WHERE NOT EXISTS (SELECT 1 FROM XSYNC.F_PLAYER_RECORDS WHERE match_id = %s)
        """
        try:
            cursor.execute(query, (record_url, match_id, ))
            cursor.execute(query_insert, (match_id, record_url, match_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True
