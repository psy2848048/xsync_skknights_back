# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback


class CouponManager:
    def __init__(self, conn):
        self.conn = conn

    def insertCouponTemplate(self, name, description):
        cursor = self.conn.cursor()

        new_coupon_id = lib.get_new_id(self.conn, "D_COUPONS")
        query = """
            INSERT INTO XSYNC.D_COUPONS
                (id, coupon_name, description)
            VALUES
                (%s, %s, %s)
        """
        try:
            cursor.execute(query, (new_coupon_id, name, description, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, None

        return True, new_coupon_id

    def deleteCouponTemplate(self, coupon_id):
        cursor = self.conn.cursor()

        query = """
            DELETE FROM XSYNC.D_COUPONS
            WHERE id = %s
        """
        try:
            cursor.execute(query, (coupon_id, ))

        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def getCouponTemplates(self):
        cursor = self.conn.cursor()

        query = """
            SELECT id as coupon_id,
                   coupon_name as name,
                   description
            FROM XSYNC.D_COUPONS
            ORDER BY id ASC
        """
        cursor.execute(query)
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return columns, ret

    def assignCouponToUser(self, coupon_id, user_id):
        cursor = self.conn.cursor()

        new_coupon_id = lib.get_new_id(self.conn, "F_COUPON_PER_USERS")
        query = """
            INSERT INTO XSYNC.F_COUPON_PER_USERS
                (id, coupon_id, user_id, is_used, issued_when)
            VALUES
                (%s, %s, %s, false, CURRENT_TIMESTAMP)
        """
        try:
            cursor.execute(query, (new_coupon_id, coupon_id, user_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, None

        return True, new_coupon_id

    def getAssignedCouponPerUser(self, user_id):
        cursor = self.conn.cursor()

        query = """
            SELECT users.id as coupon_id
                 , coupon.coupon_name as coupon_name
                 , coupon.description as coupon_description
                 , users.is_used as is_used
            FROM XSYNC.F_COUPON_PER_USERS users
            LEFT OUTER JOIN XSYNC.D_COUPONS coupon
                ON users.coupon_id = coupon.id
            WHERE users.user_id = %s
            ORDER BY users.id DESC
        """
        cursor.execute(query, (user_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return True, columns, ret

    def checkAsUsed(self, coupon_id, user_id):
        cursor = self.conn.cursor()
        query = """
            UPDATE XSYNC.F_COUPON_PER_USERS
            SET is_used = true,
                used_when = CURRENT_TIMESTAMP
            WHERE id = %s
        """
        try:
            cursor.execute(query, (coupon_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback(0)
            return False

        return True

    def deleteExpiredCoupon(self):
        cursor = self.conn.cursor()
        query = """
            DELETE FROM XSYNC.F_COUPON_PER_USERS
            WHERE issued_when < CURRENT_DATE - interval '1 days'
              AND is_used = false
        """
        try:
            cursor.execute(query)

        except Exception:
            traceback.print_exc()
            self.conn.rollback(0)
            return False

        return True

