# -*- coding: utf-8 -*-

import psycopg2

import lib
import traceback
from datetime import datetime

from touching import Touching

class PointManager:
    def __init__(self, conn, is_production=False):
        self.conn = conn
        self.touching = Touching('nGyQkfFYzG_xNXV60U89odyp3R5xVK1b', is_production=is_production)

    def _check_point(self, tel):
        inter_result = self.touching.check_point(tel)
        result = []
        for item in inter_result['milelogs']:
            temp_item = {
                    "date": item['timest'].split('T')[0]
                  , 'activity': item['log_type_str']
                  , "point": item['mile']
                    }
            result.append(temp_item)

        return result

    def check_userInfo(self, tel):
        result = self.touching.check_userInfo(tel)
        current_point_obj = result.get('milecard')
        if current_point_obj is None:
            return 0, []

        current_point = current_point_obj.get('current_mile', 0)
        earnPoint_list = self._check_point(tel)
        return current_point, earnPoint_list

    def check_exist(self, tel):
        result = self.touching.check_userInfo(tel)
        if result.get('milecard') is None:
            return False
        else:
            return True

    def save_point(self, tel, point):
        try:
            response = self.touching.save_point(tel, point)
        except:
            traceback.print_exc()
            return False
        
        return True
