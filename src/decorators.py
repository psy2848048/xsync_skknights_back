# -*- coding: utf-8 -*-
from flask import make_response
from functools import wraps

def login_required(f):
    """  
    로그인이 되어 있는지 세션 설정값을 본다.
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'is_loggedIn' in session:
            return f(*args, **kwargs)
        else:
            return make_response(json.jsonify(
                       status_code = 401, 
                       message = "Login required"
               ), 403) 

def admin_required(f):
    """  
    로그인이 되어 있는지 세션 설정값을 본다.
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('is_loggedIn', False) == True and session.get('is_admin', False) == True:
            print session.get('is_loggedIn', False)
            print session.get('is_admin', False)
            return f(*args, **kwargs)
        else:
            return make_response(json.jsonify(
                       status_code = 403, 
                       message = "Login required"
               ), 403) 
