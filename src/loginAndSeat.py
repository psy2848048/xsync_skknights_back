# -*- coding: utf-8 -*-

import psycopg2
import traceback

import lib
import csv, io


class LoginAndSeat:
    def __init__(self, conn):
        self.conn = conn

    class TouchingNotAvailableException(Exception):
        pass

    class DuplicatedSeatException(Exception):
        pass

    def _isSeatInputted(self, today_match_id, user_id):
        cursor = self.conn.cursor()

        query = """
            SELECT section, row, seat
            FROM XSYNC.F_SEAT_TABLE_PER_MATCH
            WHERE match_id = %s AND user_id = %s
            LIMIT 1
        """
        cursor.execute(query, (today_match_id, user_id, ))
        res = cursor.fetchone()

        if res is None or len(res) == 0:
            return False, None, None, None

        return True, res[0], res[1], res[2]

    def _insertUser(self, name, phoneNumber):
        cursor = self.conn.cursor()
        new_user_id = lib.get_new_id(self.conn, "D_USERS")

        query = """
            INSERT INTO XSYNC.D_USERS
                (id, phoneNumber, member_since)
            VALUES
                (%s, %s, CURRENT_TIMESTAMP)
        """
        query_update = """
            UPDATE XSYNC.D_USERS
                SET name = %s
            WHERE phoneNumber = %s
        """
        try:
            cursor.execute(query, (new_user_id, phoneNumber, ))
            cursor.execute(query_update, (name, phoneNumber, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, None

        return True, new_user_id

    def updatePushToken(self, user_id, device_category, token):
        cursor = self.conn.cursor()
        query = """
            UPDATE XSYNC.D_USERS
            SET   device_category = %s
                , push_token = %s
            WHERE id = %s
        """
        try:
            cursor.execute(query, (device_category, token, user_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def updateRecentMatch(self, user_id):
        cursor = self.conn.cursor()
        query = """
            UPDATE XSYNC.D_USERS
            SET recent_match_id = (SELECT match_id FROM XSYNC.TODAY_MATCH LIMIT 1)
            WHERE id = %s
        """
        try:
            cursor.execute(query, (user_id, ))
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def providePushTokensPerMatch(self, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT device_category, push_token
            FROM XSYNC.D_USERS
            WHERE recent_match_id = %s
        """
        try:
            cursor.execute(query, (match_id, ))
            res = cursor.fetchall()
            result = [{'device_category': device_category, 'token': token} for device_category, token in res]
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, None

        return True, result

    def providePushTokensPerUser(self, user_id):
        cursor = self.conn.cursor()
        query = """
            SELECT device_category, push_token
            FROM XSYNC.D_USERS
            WHERE id = %s
        """
        try:
            cursor.execute(query, (user_id, ))
            res = cursor.fetchone()
            device_category = res[0]
            token = res[1]
        except Exception:
            traceback.print_exc()
            self.conn.rollback()
            return False, None, None

        return True, device_category, token

    def loginAndProvideInfo(self, name, phoneNumber, device_category=None, token=None):
        is_user_exist, user_id = lib.getUserId(self.conn, name, phoneNumber)
        if is_user_exist == False:
            is_inputted, user_id = self._insertUser(name, phoneNumber)
            if is_inputted == False:
                return {
                        'is_loggedIn': False
                      , 'user_id': None
                      , 'user_name': None
                      , 'user_phone': None
                      , 'touching_id': None
                      , 'match_id': None
                      , 'is_seatInputted': False
                      , 'device_category': None
                      , 'token': None
                        }

        #touching_id = self._getTouchingUserInfo(name, phoneNumber)
        touching_id = None

        can_get_match, today_match_id = lib.getTodayMatchId(self.conn)
        is_seat_inputted, seat_section, seat_row, seat_no = self._isSeatInputted(today_match_id, user_id)

        is_inputted = self.updateRecentMatch(user_id)
        if is_inputted is False:
            raise Exception

        if device_category is not None and token is not None:
            self.updatePushToken(user_id, device_category, token)

        return {
                'is_loggedIn': True
              , 'user_id': user_id
              , 'user_name': name
              , 'user_phone': phoneNumber
              , 'touching_id': touching_id
              , 'match_id': today_match_id
              , 'is_seatInputted': is_seat_inputted
              , 'device_category': device_category
              , 'token': token
                }

    def adminLogin(self, user_name, password):
        cursor = self.conn.cursor()
        query = """
            SELECT count(*) FROM XSYNC.ADMIN_PASSWORD
            WHERE user_name = %s AND password = %s
        """
        cursor.execute(query, (user_name, password, ))
        cnt = cursor.fetchone()[0]
        if cnt == 1:
            return True
        else:
            return False

    def getSeatInfo(self, match_id, user_id):
        is_seat_inputted, seat_section, seat_row, seat_no = self._isSeatInputted(match_id, user_id)
        return is_seat_inputted, seat_section, seat_row, seat_no

    def setSeatInfo(self, match_id, user_id, section, row, seat_number):
        cursor = self.conn.cursor()

        query_delete = """
            DELETE FROM  XSYNC.F_SEAT_TABLE_PER_MATCH
            WHERE match_id = %s AND user_id = %s
        """
        query_insert = """
            INSERT INTO XSYNC.F_SEAT_TABLE_PER_MATCH
                (match_id, user_id, section, row, seat)
            VALUES
                (%s, %s, %s, %s, %s)
        """
        try:
            cursor.execute(query_delete, (match_id, user_id, ))
            cursor.execute(query_insert, (match_id, user_id, section, row, seat_number, ))

        except Exception as e:
            if 'pgcode' in e and psycopg2.errorcodes.lookup(e.pgcode):
                raise DuplicatedSeatException

            traceback.print_exc()
            self.conn.rollback()
            return False

        return True

    def exportExcel(self, match_id):
        cursor = self.conn.cursor()

        query = """
            SELECT users.name, users.phoneNumber
            FROM XSYNC.F_SEAT_TABLE_PER_MATCH matches
            LEFT OUTER JOIN XSYNC.D_USERS users
              ON matches.user_id = users.id
            WHERE matches.match_id = %s
        """

        try:
            cursor.execute(query, (match_id, ))
            res = cursor.fetchall()

        except Exception as e:
            traceback.print_exc()
            return False, None

        csv_string = ""
        for name, phoneNumber in res:
            csv_string += '"{}","{}"\n'.format(name, phoneNumber)

        return True, csv_string

    def findUserByPhone(self, phone_number, match_id):
        cursor = self.conn.cursor()
        query = """
            SELECT 
              userandmatch.user_id as user_id,
              userandmatch.name,
              userandmatch.phoneNumber,
              userandmatch.recent_match_id,
              userandmatch.match_time,
              seats.section,
              seats.row,
              seats.seat
            FROM
            (
                SELECT
                  users.id user_id,
                  users.name,
                  users.phoneNumber,
                  users.recent_match_id,
                  matches.match_time
                FROM XSYNC.D_USERS users
                LEFT OUTER JOIN XSYNC.D_MATCHES matches
                  ON users.recent_match_id = matches.id
                WHERE users.recent_match_id = %s
            ) userandmatch
            LEFT OUTER JOIN
            (
                SELECT *
                FROM XSYNC.F_SEAT_TABLE_PER_MATCH
                WHERE match_id = %s
            ) seats
              ON userandmatch.user_id = seats.user_id
            WHERE userandmatch.phoneNumber = %s
        """
        cursor.execute(query, (match_id, match_id, phone_number, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return True, columns, ret

    def findUserByName(self, name, match_id):
        cursor = self.conn.cursor()
        import urllib
        parsed_name = urllib.unquote(name)
        query = """
            SELECT 
              userandmatch.user_id as user_id,
              userandmatch.name,
              userandmatch.phoneNumber,
              userandmatch.recent_match_id,
              userandmatch.match_time,
              seats.section,
              seats.row,
              seats.seat
            FROM
            (
                SELECT
                  users.id user_id,
                  users.name,
                  users.phoneNumber,
                  users.recent_match_id,
                  matches.match_time
                FROM XSYNC.D_USERS users
                LEFT OUTER JOIN XSYNC.D_MATCHES matches
                  ON users.recent_match_id = matches.id
                WHERE users.recent_match_id = %s
            ) userandmatch
            LEFT OUTER JOIN
            (
                SELECT *
                FROM XSYNC.F_SEAT_TABLE_PER_MATCH
                WHERE match_id = %s
            ) seats
              ON userandmatch.user_id = seats.user_id
            WHERE userandmatch.name LIKE '%%{}%%'
        """.format(parsed_name.encode('utf-8'))
        cursor.execute(query, (match_id, match_id, ))
        columns = [ desc[0] for desc in cursor.description ]
        ret = cursor.fetchall()
        return True, columns, ret
