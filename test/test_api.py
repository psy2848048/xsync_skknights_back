# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import json
import sys
sys.path.append('./src')

import application


class ApplicationTestCase(TestCase):
    def _sessionMarkedAsLoggedIn(self):
        with self.app.session_transaction() as sess:
            sess['is_loggedIn'] = True
            sess['user_id'] = 1
            sess['user_name'] = u'브라이언'
            sess['user_phone'] = '01012345678'
            sess['touching_id'] = True
            sess['match_id'] = 1
            sess['is_seatInputted'] = False

    def _adminLogin(self):
        with self.app.session_transaction() as sess:
            sess['is_loggedIn'] = True
            sess['user_id'] = 10
            sess['user_name'] = u'관리자'
            sess['user_phone'] = '01012345678'
            sess['touching_id'] = '20161032adsfae'
            sess['match_id'] = 1
            sess['is_seatInputted'] = False

    def setUp(self):
        application.app.config['TESTING'] = True
        self.app = application.app.test_client()

    def tearDown(self):
        pass

    def test_get_login(self):
        res = self.app.post('/api/v1/user/login',
                data={
                    'user_name': u'브라이언'
                  , 'user_phone': '01012345678'
                    }
                )
        ret = json.loads(res.data)
        self.assertEqual(ret['is_loggedIn'], True)

    def test_seat(self):
        self._sessionMarkedAsLoggedIn()

        self.app.post('/api/v1/user/seat', data={
            'match_id': 1
          , 'section': u'가'
          , 'row': 2
          , 'seat_number': 56
            })

        ret = self.app.get('/api/v1/user/seat')
        check_data = json.loads(ret.data)
        self.assertEqual(check_data['section'], u'가')
        self.assertEqual(check_data['row'], 2)
        self.assertEqual(check_data['seat_number'], 56)

    def test_playerList(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.get('/api/v1/user/match/1/player_list')

    def test_playerProfilePic(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.get('/api/v1/user/match/1/player_list/1/profile_pic')

    def test_post_mvpVote(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.post('/api/v1/user/match/1/mvp_vote', data={
              'player_id': 1
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_get_introVideo(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.get('/api/v1/user/match/1/intro_video')
        result = json.loads(ret.data)
        self.assertEqual('video_url' in result, True)

    def test_get_teamstatus(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.get('/api/v1/user/match/1/team_status')
        result = json.loads(ret.data)
        self.assertEqual('image_url' in result, True)

    def test_get_playerstatus(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.get('/api/v1/user/match/1/player_status')
        result = json.loads(ret.data)
        self.assertEqual('image_url' in result, True)

    def test_get_event(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.get('/api/v1/user/match/1/event')
        result = json.loads(ret.data)
        self.assertEqual('events' in result, True)

    def test_post_event(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.post('/api/v1/user/match/1/event', data={
              'event_id': 1
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_get_coupon(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.get('/api/v1/user/coupon')
        result = json.loads(ret.data)
        self.assertEqual('coupon_id' in result[0], True)

    def test_post_coupon(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.post('/api/v1/user/coupon', data={
              'coupon_id': 1
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_jukebox(self):
        self._sessionMarkedAsLoggedIn()
        ret = self.app.post('/api/v1/user/match/1/jukebox', data={
              'name': u'이준행'
            , 'description': u'지금까지 다니던 회사를 그만두고 창업을 했습니다. 이 노래와 함게 SK도 승리하고 제 일도 술술 풀렸으면 좋겠습니다!'
            , 'song': u'김동률/출발'
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_selfie(self):
        self._sessionMarkedAsLoggedIn()
        f = open('testdata/test_pic.png', 'r')
        ret = self.app.post('/api/v1/user/match/1/selfie', data={
            "image_bin": f.read()
            }
                )
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_get_admin_match(self):
        self._adminLogin()
        ret = self.app.get('/api/v1/admin/match')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_post_admin_match(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/match', data={
              'date': '2016-10-30'
            , 'versus': '원주'
            })
        result = json.loads(ret.data)
        self.assertEqual('match_id' in result, True)

    def test_put_admin_match(self):
        self._adminLogin()
        ret = self.app.put('/api/v1/admin/match/1', data={
              'date': '2016-10-29'
            , 'versus': '원주'
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_admin_match(self):
        self._adminLogin()
        ret = self.app.delete('/api/v1/admin/match/1')
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_team_status(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/match/1/team_status',
                data={'image_url': 'http://blahblah.me/1/team'})
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_player_status(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/match/1/player_status',
                data={'image_url': 'http://blahblah.me/1/player'})
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_introMovie(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/match/1/intro_video',
                data={'video_url': 'http://blahblah.me/1/video'})
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_get_couponTemplate(self):
        self._adminLogin()
        ret = self.app.get('/api/v1/admin/coupon/template')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_post_couponTemplate(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/coupon/template', data={
              'name': '선수 지목 1명 키스'
            , 'description': '원하는 선수 1명 지목해서 키스할 수 있는 이벤트'
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_assignCoupon(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/coupon', data={
              'numbers': str(['01087654321', '01012345678', '01023456789'])
            , 'coupon_id': 1
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_get_eventTemplate(self):
        self._adminLogin()
        ret = self.app.get('/api/v1/admin/event/template')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_post_eventTemplate(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/event/template', data={
              'name': '선수 지목 1명 키스'
            , 'description': '원하는 선수 1명 지목해서 키스할 수 있는 이벤트'
            , 'category': '추첨'
            , 'point_needed': 5000
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_put_eventTemplate(self):
        self._adminLogin()
        ret = self.app.put('/api/v1/admin/event/template/1', data={
              'name': '선수 지목 1명 키스'
            , 'description': '원하는 선수 1명 지목해서 키스할 수 있는 이벤트'
            , 'category': '추첨'
            , 'point_needed': 5000
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_delete_eventTemplate(self):
        self._adminLogin()
        ret = self.app.delete('/api/v1/admin/event/template/1')
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_get_eventOfMatch(self):
        self._adminLogin()
        ret = self.app.get('/api/v1/admin/match/1/event')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_post_eventOfMatch(self):
        self._adminLogin()
        ret = self.app.get('/api/v1/admin/match/1/event')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_post_assignEventOfMatch(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/match/1/event/1/assign')
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_startEventOfMatch(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/match/1/event/1/start')
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_closeEventOfMatch(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/match/1/event/1/close')
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_post_deleteEventOfMatch(self):
        self._adminLogin()
        ret = self.app.delete('/api/v1/admin/match/1/event/1/delete')
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_get_checkJukebox(self):
        self._adminLogin()
        ret = self.app.get('/api/v1/admin/match/1/jukebox')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_get_mvpResult(self):
        self._adminLogin()
        ret = self.app.get('/api/v1/admin/match/1/mvp_vote')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_get_wholePlayer(self):
        self._adminLogin()
        ret = self.app.get('/api/v1/admin/player')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_post_wholePlayer(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/player', data={
              'position': '가드'
            , 'number': '00'
            , 'name': '강창구'
            , 'height': 190
            , 'weight': 102
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_put_wholePlayer(self):
        self._adminLogin()
        ret = self.app.put('/api/v1/admin/player/1', data={
              'position': '가드'
            , 'number': '00'
            , 'name': '강창구'
            , 'height': 190
            , 'weight': 102
            })
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_delete_wholePlayer(self):
        self._adminLogin()
        ret = self.app.delete('/api/v1/admin/player/1')
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_get_matchPlayer(self):
        self._adminLogin()
        ret = self.app.delete('/api/v1/admin/match/1/players/1')
        result = json.loads(ret.data)
        self.assertEqual('data' in result, True)

    def test_post_matchPlayer(self):
        self._adminLogin()
        ret = self.app.post('/api/v1/admin/match/1/players', data={'player_id': 1})
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)

    def test_delete_matchPlayer(self):
        self._adminLogin()
        ret = self.app.delete('/api/v1/admin/match/1/players/1')
        result = json.loads(ret.data)
        self.assertEqual('message' in result, True)
