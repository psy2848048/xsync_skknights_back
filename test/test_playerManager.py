# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from playerManager import PlayerManager


class LoginAndSeatTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.playerManagerObj = PlayerManager(self.conn)

    def tearDown(self):
        self.conn.rollback()

    def test_playerInsertAndSelect(self):
        is_succeeded, new_player_id = self.playerManagerObj.insertPlayer(u'가드', '01', u'진성화', 185, 90)
        self.assertEqual(is_succeeded, True)

        player_id, position, number, name, height, weight = self.playerManagerObj.getSpecificPlayerList(new_player_id)
        self.assertEqual(
                (position, number, name, height, weight)
              , (u'가드', '01', u'진성화', 185, 90)
              )

    def test_getWholePlayerList(self):
        res = self.playerManagerObj.getWholePlayerList()

    def test_playerProfilePhoto(self):
        is_succeeded, new_player_id = self.playerManagerObj.insertPlayer(u'가드', '01', u'진성화', 185, 90)

        pic_file = open('test/testdata/test_pic.png', 'r')
        image_bin = pic_file.read()
        is_succeeded = self.playerManagerObj.setPlayerProfilePhoto(new_player_id, 'test/testdata/test_pic.png', image_bin)

        is_succeeded, player_pic = self.playerManagerObj.getPlayerProfilePhoto(new_player_id)
        self.assertEqual(image_bin, player_pic)
        pic_file.close()

    def test_getLineUpForSpecificMatch(self):
        is_succedded, columns, ret = self.playerManagerObj.getLineUpForSpecificMatch(1)

    def test_setLineUpForSpecificMatch(self):
        is_succedded = self.playerManagerObj.setLineUpForSpecificMatch(1, 1)

    def test_deleteLineUpForSpecificMatch(self):
        is_succedded = self.playerManagerObj.deleteLineUpForSpecificMatch(1, 1)

