# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from mvpVote import MvpVote
from loginAndSeat import LoginAndSeat
from matchManager import MatchManager
from playerManager import PlayerManager


class MvpVoteTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.mvpVoteObj = MvpVote(self.conn)
        self.loginManager = LoginAndSeat(self.conn)
        self.matchManager = MatchManager(self.conn)
        self.playerManager = PlayerManager(self.conn)

        is_inserted, self.match_id = self.matchManager.addMatch('원주', '2016-09-25')
        is_set = self.matchManager.setTodayMatch(self.match_id)

        is_inserted, self.player_id1 = self.playerManager.insertPlayer('가드', '00', '이명준', 190, 102)
        is_inserted, self.player_id2 = self.playerManager.insertPlayer('세터', '12', '붸엙', 190, 102)
        is_set = self.playerManager.setLineUpForSpecificMatch(self.match_id, self.player_id1)
        is_set = self.playerManager.setLineUpForSpecificMatch(self.match_id, self.player_id2)

        is_inserted, self.user_id1 = self.loginManager._insertUser('아그래', '01012345678')
        is_inserted, self.user_id2 = self.loginManager._insertUser('장그래', '01023456789')

    def tearDown(self):
        self.conn.rollback()

    def test_voteMvp(self):
        is_voted = self.mvpVoteObj.voteMvp(self.user_id1, self.match_id, self.player_id1)

    def test_checkVote(self):
        is_voted = self.mvpVoteObj.voteMvp(self.user_id1, self.match_id, self.player_id1)
        is_voted = self.mvpVoteObj.voteMvp(self.user_id2, self.match_id, self.player_id2)

        columns, ret = self.mvpVoteObj.checkVote(self.match_id)
        self.assertEqual(len(ret), 2)

