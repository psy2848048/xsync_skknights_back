# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from loginAndSeat import LoginAndSeat
from matchManager import MatchManager


class LoginAndSeatTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.loginAndSeatObj = LoginAndSeat(self.conn)
        self.matchManagerObj = MatchManager(self.conn)

    def tearDown(self):
        self.conn.rollback()

    def test_loginAndProvideInfo(self):
        login_obj = self.loginAndSeatObj.loginAndProvideInfo(u'브라이언', '01012345678', device_category='android', token='01234567890asdfasefawefaerg')

        is_match_inserted, match_id = self.matchManagerObj.addMatch('원주', '2016-10-21')
        is_set = self.matchManagerObj.setTodayMatch(match_id)
        self.assertEqual(login_obj['is_loggedIn'], True)
        self.assertEqual(login_obj['device_category'], 'android')
        self.assertEqual(login_obj['token'], '01234567890asdfasefawefaerg')

        is_seat_input_successful = self.loginAndSeatObj.setSeatInfo(match_id, login_obj['user_id'], 1, '가', 12)
        all_finished_user_obj = self.loginAndSeatObj.loginAndProvideInfo('브라이언', '01012345678')
        self.assertEqual(all_finished_user_obj['is_seatInputted'], True)

    def test_getSeatInfo(self):
        is_seat_inputted, seat_section, seat_row, seat_no = self.loginAndSeatObj.getSeatInfo(1, 1)
        self.assertEqual(
                    (is_seat_inputted, seat_section, seat_row, seat_no)
                  , (False, None, None, None)
                )

        is_match_inserted, match_id = self.matchManagerObj.addMatch('원주', '2016-10-21')
        is_changed = self.matchManagerObj.setTodayMatch(match_id)
        is_inserted, user_id = self.loginAndSeatObj._insertUser('브라이언', '01012345678')
        is_seat_input_successful = self.loginAndSeatObj.setSeatInfo(match_id, user_id, 1, '가', 12)
        self.assertEqual(is_seat_input_successful, True)
        is_seat_inputted, seat_section, seat_row, seat_no = self.loginAndSeatObj.getSeatInfo(match_id, user_id)
        self.assertEqual(
                    (is_seat_inputted, seat_section, seat_row, seat_no)
                  , (True, '1', '가', '12')
                )

    def test_providePushTokensPerMatch(self):
        is_match_inserted, match_id = self.matchManagerObj.addMatch('원주', '2016-10-21')
        is_changed = self.matchManagerObj.setTodayMatch(match_id)
        login_obj = self.loginAndSeatObj.loginAndProvideInfo(u'브라이언', '01012345678', device_category='android', token='01234567890asdfasefawefaerg')
        match_id = login_obj['match_id']
        can_get, res = self.loginAndSeatObj.providePushTokensPerMatch(match_id)
        self.assertEqual(len(res), 1)

    def test_providePushTokensPerUser(self):
        is_match_inserted, match_id = self.matchManagerObj.addMatch('원주', '2016-10-21')
        is_changed = self.matchManagerObj.setTodayMatch(match_id)
        login_obj = self.loginAndSeatObj.loginAndProvideInfo(u'브라이언', '01012345678', device_category='android', token='01234567890asdfasefawefaerg')
        user_id = login_obj['user_id']
        can_get, device_category, token = self.loginAndSeatObj.providePushTokensPerUser(user_id)
        self.assertEqual(token, '01234567890asdfasefawefaerg')
