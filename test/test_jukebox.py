# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from jukebox import Jukebox


class JukeboxTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.jukeboxObj = Jukebox(self.conn)

    def tearDown(self):
        self.conn.rollback()

    def test_jukeboxCR(self):
        is_requested = self.jukeboxObj.requestSong(1, u'이준행', u'힘내세요', u'노브레인/ 개가 개를 먹는도다')
        ret = self.jukeboxObj.checkJukebox(1)
        self.assertEqual(1, len(ret))
