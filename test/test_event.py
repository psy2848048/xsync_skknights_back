# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from event import Event, NotOpenedException
from matchManager import MatchManager
from loginAndSeat import LoginAndSeat


class EventTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.eventObj = Event(self.conn)
        self.matchManagerObj = MatchManager(self.conn)
        self.loginManager = LoginAndSeat(self.conn)

        is_inserted, self.user_id = self.loginManager._insertUser('브라이언', '01012345678')
        is_inserted, self.match_id = self.matchManagerObj.addMatch('나선', '2016-11-01')
        is_set = self.matchManagerObj.setTodayMatch(self.match_id)

    def tearDown(self):
        self.conn.rollback()

    def test_CRUDEventTemplate(self):
        # C
        in_insert_succeeded, item_id1 = self.eventObj.insertEventTemplate(u'동전쌓기', u'선착순', 4000, u'동전 세워서 높이 쌓는 분 승리')
        in_insert_succeeded, item_id2 = self.eventObj.insertEventTemplate(u'도장깨기', u'선착순', 5000, u'경기 중 상대방 벤치에 난입하여 상대팀 멘탈을 흐뜨러뜨리기. ')

        # R
        columns, ret = self.eventObj.getEventTemplateList()
        self.assertEqual(len(ret), 2)
        self.assertEqual(
                (ret[0][0], ret[1][0])
              , (item_id1, item_id2)
              )

        # U
        is_updated = self.eventObj.updateEventTemplate(item_id2, **{"choice_method": u"체급순"})
        columns, ret = self.eventObj.getEventTemplateList()
        self.assertEqual(ret[1][2], '체급순')

        # D
        is_deleted = self.eventObj.deleteEventTemplate(item_id2)
        columns, ret = self.eventObj.getEventTemplateList()
        self.assertEqual(len(ret), 1)

    def test_CRUDEventOfMatchAndParticipate(self):
        in_insert_succeeded, item_id1 = self.eventObj.insertEventTemplate(u'동전쌓기', u'선착순', 4000, u'동전 세워서 높이 쌓는 분 승리')
        in_insert_succeeded, item_id2 = self.eventObj.insertEventTemplate(u'도장깨기', u'선착순', 5000, u'경기 중 상대방 벤치에 난입하여 상대팀 멘탈을 흐뜨러뜨리기. ')

        # Assigning Event
        # C
        is_succeeded = self.eventObj.assignEventToMatch(self.match_id, item_id1)
        is_succeeded = self.eventObj.assignEventToMatch(self.match_id, item_id2)

        # R
        can_get, columns, ret = self.eventObj.getEventsOfMatch(self.match_id)
        self.assertEqual(len(ret), 2)

        # D
        is_deleted = self.eventObj.unassignEventToMatch(self.match_id, item_id2)
        can_get, columns, ret = self.eventObj.getEventsOfMatch(self.match_id)
        self.assertEqual(len(ret), 1)

        # Set as Open
        is_opened = self.eventObj.openEvent(self.match_id, item_id1)
        can_get, columns, ret = self.eventObj.getEventsOfMatch(self.match_id)
        self.assertEqual(ret[0][5], True)

        # Set as Closed
        is_closed = self.eventObj.closeEvent(self.match_id, item_id1)
        can_get, columns, ret = self.eventObj.getEventsOfMatch(self.match_id)
        self.assertEqual(ret[0][5], False)

        # Join to event
        is_exception_passed = False
        try:
            is_joined = self.eventObj.joinEvent(self.match_id, item_id1, 1)
        except NotOpenedException:
            is_exception_passed = True

        if is_exception_passed == False:
            raise Exception('Event open/close function is not working!')

        is_opened = self.eventObj.openEvent(self.match_id, item_id1)
        is_joined = self.eventObj.joinEvent(self.match_id, item_id1, self.user_id)

        columns, ret = self.eventObj.getJoinedEvents(self.match_id, item_id1)
        self.assertEqual(len(ret), 1)
        self.assertEqual(ret[0][0], self.user_id)

