# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from coupon import CouponManager
from loginAndSeat import LoginAndSeat


class CouponManagerTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.couponObj = CouponManager(self.conn)
        self.loginManager = LoginAndSeat(self.conn)

    def tearDown(self):
        self.conn.rollback()

    def test_insertCouponTemplate(self):
        is_inserted, new_coupon_template_id1 = self.couponObj.insertCouponTemplate('선수와 키스하기', '쿠폰소지자가 지목하는 선수와 1회 키스 이용권')
        is_inserted, new_coupon_template_id2 = self.couponObj.insertCouponTemplate('버블헤드', '원하는 선수의 버블헤드 수령')

        ret = self.couponObj.getCouponTemplates()
        self.assertEqual(2, len(ret))

        is_deleted = self.couponObj.deleteCouponTemplaste(new_coupon_template_id1)
        column, ret = self.couponObj.getCouponTemplates()
        self.assertEqual(1, len(ret))

        is_registered, user_id = self.loginManager._insertUser('브라이언', '01012345678')
        is_assigned, coupon_id = self.couponObj.assignCouponToUser(new_coupon_template_id2, user_id)
        can_get, columns, ret = self.couponObj.getAssignedCouponPerUser(user_id)
        self.assertEqual(1, len(ret))

        is_used = self.couponObj.checkAsUsed(coupon_id, user_id)
        can_get, columns, ret = self.couponObj.getAssignedCouponPerUser(user_id)
        self.assertEqual(True, ret[0][3])
