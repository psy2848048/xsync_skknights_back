# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from touching import Touching


class TouchingTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.touchingObj = Touching('p2hcblWwXOncD_YZbZv7m79yK4VeM4Qa')

    def tearDown(self):
        self.conn.rollback()

    def test_getPointList(self):
        result = self.touchingObj.check_point('01012345678')
