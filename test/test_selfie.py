# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from selfie import Selfie
from matchManager import MatchManager
from loginAndSeat import LoginAndSeat


class SelfieTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.selfieObj = Selfie(self.conn)
        self.matchManager = MatchManager(self.conn)
        self.loginManager = LoginAndSeat(self.conn)

        is_inserted, self.match_id = self.matchManager.addMatch('원주', '2016-10-26')
        is_set = self.matchManager.setTodayMatch(self.match_id)

        is_inserted, self.user_id = self.loginManager._insertUser('아그래', '01012345678')

    def tearDown(self):
        self.conn.rollback()

    def test_insertSelfie(self):
        f = open('./test/testdata/test_pic.png')
        image_bin = f.read()

        is_inserted = self.selfieObj.insertSelfie(self.match_id, 'png', self.user_id, image_bin)
        image_ext, uploaded_image_bin = self.selfieObj.getSelfieBinary(self.match_id, self.user_id)
        self.assertEqual(bytearray(image_bin), uploaded_image_bin)

        column, res = self.selfieObj.getSelfieList(self.match_id)
