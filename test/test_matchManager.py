# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from matchManager import MatchManager


class MatchManagerTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.matchManagerObj = MatchManager(self.conn)

    def tearDown(self):
        self.conn.rollback()

    def test_addMatch(self):
        is_added, match_id = self.matchManagerObj.addMatch(u'동양 오리온스', '07-10')

    def test_setTodayMatch(self):
        is_set = self.matchManagerObj.setTodayMatch(1)

    def test_deleteMatch(self):
        is_added, match_id = self.matchManagerObj.addMatch(u'동양 오리온스', '07-10')
        is_added, match_id = self.matchManagerObj.addMatch(u'원주 나래 해커스', '07-11')
        is_deleted = self.matchManagerObj.deleteMatch(1)
