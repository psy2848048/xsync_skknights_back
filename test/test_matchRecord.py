# -*- coding: utf-8 -*-

from unittest import TestCase
import psycopg2
import sys
sys.path.append('./src')

from matchRecord import MatchRecord
from matchManager import MatchManager


class MatchRecordTestCase(TestCase):
    def setUp(self):
        self.conn = psycopg2.connect('host=localhost port=5432 dbname=xsync user=xsync password=postgres')
        self.matchRecordObj = MatchRecord(self.conn)
        self.matchManagerObj = MatchManager(self.conn)

        is_inserted, self.match_id = self.matchManagerObj.addMatch('나선', '2016-11-01')
        is_set = self.matchManagerObj.setTodayMatch(self.match_id)

    def tearDown(self):
        self.conn.rollback()

    def test_setAndGetTeamRecord(self):
        is_set = self.matchRecordObj.setTeamRecord(self.match_id, 'http://blahblah.com/1.jpg')
        is_get, image_url = self.matchRecordObj.getTeamRecord(self.match_id)
        self.assertEqual(image_url, 'http://blahblah.com/1.jpg')

    def test_setAndGetPlayerRecord(self):
        is_set = self.matchRecordObj.setPlayerRecord(self.match_id, 'http://blahblah.com/2.jpg')
        is_get, image_url = self.matchRecordObj.getPlayerRecord(self.match_id)
        self.assertEqual(image_url, 'http://blahblah.com/2.jpg')
