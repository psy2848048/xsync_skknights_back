from distutils.core import setup

setup(
        name='SK Knights'
      , version='0.9'
      , author='Bryan RHEE'
      , author_email='junhang.lee@ciceeron.me'
      , packages=['src']
      , install_requires=[
                        "pytest"
                      , "Flask"
                      , "requests"
                      , "flask-cors"
                      , "flask-redis"
                      , "flask-session"
                      , "psycopg2"
                      , "redis"
         ])
